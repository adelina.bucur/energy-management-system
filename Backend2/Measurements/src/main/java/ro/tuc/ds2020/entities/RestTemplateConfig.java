package ro.tuc.ds2020.entities;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.List;

@Configuration
public class RestTemplateConfig {

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    public ResponseEntity<List> getForEntity(String url, Class<List> listClass) {

        RestTemplate restTemplate = restTemplate();
        return restTemplate.getForEntity(url, listClass);
    }

  

    public ResponseEntity<Double> exchange(String url, HttpMethod get, Object o, Class<Double> doubleClass) {
        RestTemplate restTemplate = restTemplate();
        HttpMethod method = null;
        return restTemplate.exchange(url, HttpMethod.GET, null, Double.class);
    }
}