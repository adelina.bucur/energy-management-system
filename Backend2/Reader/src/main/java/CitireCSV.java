import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class CitireCSV {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(new File("C:\\Users\\Adelina Bucur\\Desktop\\DS2023_32721_Bucur_Adelina_Assignment_2\\Sensor\\sensor.csv"));
            sc.useDelimiter(",");

            while (sc.hasNext()) {
                System.out.print(sc.next());
            }

            sc.close(); // closes the scanner
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
