import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import org.json.JSONObject;


import javax.sound.midi.spi.SoundbankReader;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Properties;

public class Send {

    private final static String QUEUE_NAME = "demoqueue";

    public static void main(String[] argv) throws Exception {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setUri("amqps://picmlfpv:GDdELxPL_ikAFETcWOB6JZVO32eHTivy@moose.rmq.cloudamqp.com/picmlfpv");

        try (Connection connection = factory.newConnection();
             Channel channel = connection.createChannel()) {

            String csvFilePath = "C:\\Users\\Adelina Bucur\\Desktop\\DS2023_32721_Bucur_Adelina_Assignment_2\\Sensor\\sensor.csv";

            try (BufferedReader br = new BufferedReader(new FileReader(csvFilePath))) {
                String line;
                while ((line = br.readLine()) != null) {
                    // Assuming each line of the CSV file is a separate message
                    ObjectWriter ow = new ObjectMapper().writer().withDefaultPrettyPrinter();
                    String json = ow.writeValueAsString(line);
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("value", line);

                    Properties properties = new Properties();
                    try (FileInputStream propFile = new FileInputStream("id.txt")) {
                        properties.load(propFile);
                    } catch (Exception exception) {
                        exception.printStackTrace();
                    }
                    String idValue = properties.getProperty("id");
                    jsonObject.put("id_device", idValue);

                    System.out.println(jsonObject);

                    String jsonObjectString = jsonObject.toString();

                    channel.queueDeclare(QUEUE_NAME, true, false, false, null);

                    channel.basicPublish("", QUEUE_NAME, null, jsonObjectString.toString().getBytes(StandardCharsets.UTF_8));

                    try {
                        Thread.sleep(10000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }


        }

        /*
        {
            value: "0",
            id_device: "5"

        }
         */
    }
}

