//package ro.tuc.ds2020;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//@Service
//public class MessageService {
//
//    @Autowired
//    private IMessageRepository messageRepository; // Assuming you have a JPA repository for Message
//
//    // Save a new message
//    public void saveMessage(Message message) {
//        // Add logic to save the message
//        // For example, using a JPA repository:
//        messageRepository.save(message);
//    }
//
//    // Mark a message as seen
//    public void markMessageAsSeen(int messageId) {
//        // Retrieve the message from the database
//        // Update its status to 'seen'
//        // Save the updated message
//        // This is a simplified example. In a real application, you'd handle exceptions and edge cases.
//        Message message = messageRepository.findById(messageId).orElse(null);
//        if (message != null) {
//            //message.isRead(true); // Assuming 'setRead' method sets the 'isRead' field to true
//            messageRepository.save(message);
//        }
//    }
//
//    // Other methods related to message handling can be added here
//}
