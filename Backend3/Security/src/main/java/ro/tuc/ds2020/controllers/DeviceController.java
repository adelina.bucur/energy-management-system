package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.entities.Device;
import ro.tuc.ds2020.entities.Person;
//import ro.tuc.ds2020.repositories.IDeviceRepository;
//import ro.tuc.ds2020.services.DeviceService;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "/device")
public class DeviceController {
//    @Autowired
//    DeviceService deviceService;
    private final String authServiceUrl = "http://localhost:8081/device";
    private final String personURL = "http://localhost:8080/person";
    @RequestMapping (method = RequestMethod.GET, value = "/auth/all")
    @ResponseBody
    public List<Device> getAllDevices() {
        String url = authServiceUrl + "/all";
        ResponseEntity<Device[]> response = restTemplate.exchange(url, HttpMethod.GET, null, Device[].class);
        return Arrays.asList(response.getBody());
    }

    @RequestMapping(method = RequestMethod.POST, value = "/auth/save")
    @ResponseBody
    public Device saveDevice(@RequestBody Device device) {
        String url = authServiceUrl + "/save";
        ResponseEntity<Device> response = restTemplate.postForEntity(url, device, Device.class);
        return response.getBody();
    }



    @RequestMapping (method = RequestMethod.DELETE, value = "/auth/delete")
    @ResponseBody
    public ResponseEntity<String> deleteDeviceById(@RequestParam(name = "id") Integer id) {
        String url = authServiceUrl + "/delete?id=" + id;
        restTemplate.exchange(url, HttpMethod.DELETE, null, String.class);
        return ResponseEntity.ok("Device deleted successfully.");
    }


    @Autowired
    public RestTemplate restTemplate;
    @RequestMapping(method = RequestMethod.GET, value = "/auth/devByUser")
    @ResponseBody
    public List<Device> devByUser(@RequestParam(name = "idClient") Integer idClient) {
        String url = authServiceUrl + "/devByUser?idClient=" + idClient;
        ResponseEntity<Device[]> devicesForClient = restTemplate.exchange(url, HttpMethod.GET, null, Device[].class);

        return Arrays.asList(devicesForClient.getBody());
    }

    @RequestMapping(method = RequestMethod.GET, value = "/auth/devByUsername")
    @ResponseBody
    public List<Device> devByUsername(@RequestParam(name = "username") String username) {
        String url = personURL + "/getByUsername?username=" + username;

        ResponseEntity<Person> response = restTemplate.exchange(url, HttpMethod.GET, null, Person.class);

        if (response.getStatusCode() == HttpStatus.OK) {
            Person person = response.getBody();
            if (person != null) {
                int idPers = person.getId();
                String url2 = authServiceUrl + "/devByUser?idClient=" + idPers;
                ResponseEntity<Device[]> deviceResponse = restTemplate.exchange(url2, HttpMethod.GET, null, Device[].class);

                if (deviceResponse.getBody() != null) {

                    return Arrays.asList(deviceResponse.getBody());
                } else {
                    throw new NoSuchElementException("No devices found for person with ID: " + idPers);
                }
            } else {
                throw new NoSuchElementException("No person found with username: " + username);
            }
        } else {
            throw new RuntimeException("Failed to fetch person. Status code: " + response.getStatusCode());
        }
    }
}
