package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.entities.Measurement;
import org.springframework.web.client.RestTemplate;


import java.util.Arrays;
import java.util.Date;
import java.util.List;



@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "/measurement")
public class MeasurementController {
    private final String deviceUrl = "http://localhost:8081/device";

    @Autowired
    private RestTemplate restTemplate;
    private final String authServiceUrl = "http://localhost:8082/measurement";
    @RequestMapping(method = RequestMethod.GET, value = "/auth/all")
    @ResponseBody
    public ResponseEntity<List<Measurement>> getAllAuth() {
        ResponseEntity<Measurement[]> response = restTemplate.exchange(
                authServiceUrl + "/all",
                HttpMethod.GET,
                null,
                Measurement[].class
        );
        return ResponseEntity.ok(Arrays.asList(response.getBody()));
    }

    @RequestMapping(method = RequestMethod.GET, value = "/auth/getMaxHourFromDevice")
    @ResponseBody
    public List<Double> getMaxHourFromDevice(@RequestParam(name = "idDevice") Integer id) {
        String url = authServiceUrl + "/getMaxHourFromDevice?idDevice=" + id;
        System.out.println(url);
        ResponseEntity<List> responseEntity = restTemplate.getForEntity(url, List.class);

        if (responseEntity.getStatusCode().is2xxSuccessful()) {
            return responseEntity.getBody();
        } else {

            return null;
        }
    }
    @RequestMapping(method = RequestMethod.GET, value = "/auth/getSum")
    @ResponseBody
    public Double getSum(@RequestParam(name = "idDevice") Integer id) {
        String url = authServiceUrl + "/getSum?idDevice=" + id;

        // Make a GET request to the external service to calculate the sum for the last hour
        ResponseEntity<Double> responseEntity = restTemplate.getForEntity(url, Double.class);

        if (responseEntity.getStatusCode() == HttpStatus.OK && responseEntity.getBody() != null) {
            // Return the calculated sum from the response
            return responseEntity.getBody();
        } else {
            // Handle the case where no sum is returned or there is an error
            throw new RuntimeException("Failed to calculate sum or bad response. Status code: " + responseEntity.getStatusCode());
        }
    }


}
