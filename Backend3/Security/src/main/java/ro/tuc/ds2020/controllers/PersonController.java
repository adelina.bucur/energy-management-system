package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.security.JwtTokenUtil;
import ro.tuc.ds2020.security.JwtUserDetailsService;

import ro.tuc.ds2020.security.JwtRequest;
import ro.tuc.ds2020.security.JwtResponse;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "*")
@RestController
@RequestMapping(value = "/person")
public class PersonController {

    private final RestTemplate restTemplate;
    private final String authServiceUrl = "http://localhost:8080/person";


    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private JwtUserDetailsService userDetailsService;

    private AuthenticationManager authenticationManager = new AuthenticationManager() {
        @Override
        public Authentication authenticate(Authentication authentication) throws AuthenticationException {
            return null;
        }
    };

    public PersonController(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }


    // New method for creating JWT token after successful authentication
    //@PreAuthorize("role")
    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
        authenticate(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        System.out.println("tzeapa");
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getUsername());

        final String token = jwtTokenUtil.generateToken(userDetails); // Pass userDetails to include roles in token

        return ResponseEntity.ok(new JwtResponse(token));
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

   // @PreAuthorize("hasRole('ROLE_ADMIN')")
    @RequestMapping(method = RequestMethod.GET, value = "/auth/all")
    public List<Person> getAll() {
        Person[] people = restTemplate.exchange("http://localhost:8080/person/all", HttpMethod.GET, null, Person[].class).getBody();
        return Arrays.asList(people);
    }
    @RequestMapping(method = RequestMethod.POST, value = "/auth/save")
    @ResponseBody
    public ResponseEntity<Person> savePerson(@RequestBody Person person) {
        ResponseEntity<Person> response = restTemplate.postForEntity(
                authServiceUrl + "/save", person, Person.class);
        return response;
    }

    @RequestMapping(method = RequestMethod.POST, value="/auth/update")
    @ResponseBody
    public ResponseEntity<Person> updatePerson(@RequestParam(name="id") Integer id, @RequestParam(name="username") String username, @RequestParam(name="password") String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Creating a map for the parameters
        Map<String, Object> map = new HashMap<>();
        map.put("id", id);
        map.put("username", username);
        map.put("password", password);

        HttpEntity<Map<String, Object>> requestEntity = new HttpEntity<>(map, headers);

        // Assuming 'authServiceUrl' is a URL to your authentication service
        return restTemplate.exchange(
                authServiceUrl + "/update",
                HttpMethod.POST,
                requestEntity,
                Person.class);
    }

    @RequestMapping(method = RequestMethod.DELETE, value="/auth/delete")
    @ResponseBody
    public ResponseEntity<String> deleteAuthById(@RequestParam(name = "id") Integer id) {
        restTemplate.exchange(authServiceUrl + "/delete?id=" + id, HttpMethod.DELETE, null, String.class);
        return ResponseEntity.ok("Person deleted successfully.");
    }



}

