import React from 'react';
import { useLocation, Navigate } from 'react-router-dom';

export function PrivateRoute({ element }) {
  const location = useLocation();


  const userData = localStorage.getItem('userLoginData');


  if (userData) {
  
    return element;
  } else {

    return <Navigate to="/" state={{ from: location }} />;
  }
}

export default PrivateRoute;

