package ro.tuc.ds2020.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import ro.tuc.ds2020.entities.Person;
import ro.tuc.ds2020.services.PersonService;

import java.io.IOException;
import java.util.List;

@CrossOrigin(origins = "*")
@Controller
@RequestMapping(value = "/person")
@RestController
public class PersonController {
    @Autowired
    PersonService personService;
    @Autowired
    private RestTemplate restTemplate; // Configurați un bean RestTemplate în configurația dvs.


    @RequestMapping (method = RequestMethod.GET, value = "/all")
    @ResponseBody
    public List<Person> getAll(){
        return personService.getAll();
    }
    @RequestMapping(method = RequestMethod.POST, value = "/save")
    @ResponseBody
    public Person savePerson(@RequestBody Person person) throws IOException{
         return personService.savePerson(person);
    }

    @RequestMapping(method = RequestMethod.POST, value="/update")
    @ResponseBody
    public Person updatePerson(@RequestParam(name="id") Integer id,@RequestParam(name="username") String username, @RequestParam(name="password") String password) throws IOException{

         return personService.updatePerson(id, username, password);
    }

    @RequestMapping (method = RequestMethod.GET, value = "/getbyId")
    @ResponseBody
    public Person getbyId(@RequestParam(name="id") Integer id ){
        return personService.getbyId(id);
    }

        @RequestMapping (method = RequestMethod.DELETE, value = "/delete")
        @ResponseBody
        public void delete(@RequestParam(name="id") Integer id){
          personService.delete(id);

    }

    @RequestMapping (method = RequestMethod.GET, value = "/getByUsernameAndPassword")
    @ResponseBody
    public Person getByUsernameAndPassword(@RequestParam(name="username") String username, @RequestParam(name="password") String password  ){
        return personService.getByUsernameAndPassword(username, password);
    }
    @RequestMapping (method = RequestMethod.GET, value = "/getByUsername")
    @ResponseBody
    public Person getByUsername(@RequestParam(name="username") String username ){
        return personService.getByUsername(username);
    }
    @RequestMapping (method = RequestMethod.DELETE, value = "/deleteUser")
    @ResponseBody
    public void deleteUser(@RequestParam(name = "idClient") Integer idClient) {

        String deviceServiceUrl = "http://localhost:8081/device/devByUser?idClient=" + idClient;
        restTemplate.delete(deviceServiceUrl);

        personService.delete(idClient);

    }


}
